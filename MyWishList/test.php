<?php

require 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\Modeles\Liste;
use mywishlist\Modeles\Item;


$config = parse_ini_file('dbconf.ini');
$db=new DB();
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();


$listes=Liste::get();
echo "<h2>Lister les listes de souhait</h2><br>";
foreach($listes as $liste){
	echo "Titre :".$liste->titre ."<br>  Description : ".$liste->description."<br><br>";
}
$items=Item::get();

echo "<h2>Lister les items</h2><br>";
foreach($items as $item){
	echo "Nom :".$item->nom ."<br>  Description : ".$item->descr."<br>";
	if($item->liste_id!=null){
		echo "liste : ".$item->liste->titre . "<br><br>";
	}
	else{
		echo "liste : null <br><br>";
	}
}

if(array_key_exists("id",$_GET)){
	$resultItem=Item::select('nom','descr')->where('id','=',$_GET['id'])->first();
	echo "<h2>Item dont l'id est passé en paramètre :". $_GET['id']."</h2><br>";
	echo "Nom :".$resultItem->nom ."<br>  Description : ".$resultItem->descr."<br><br>";
	
}
else {
	echo "<h2>Item dont l'id est passé en paramètre :";
	echo "pas d'id en paramètre dans l'url <br>";
}

if(Item::select('nom')->where('id','=',30)->first()==null){
	$i=new Item();
	$i->id=30;
	$i->liste_id=3;
	$i->nom="Iphone X";
	$i->descr="Un truc vraiment cher";
	$i->save();
}


echo "<h2>Lister les items de la liste 3 avec association hasMany</h2><br>";

$listeItems = $liste->items;
foreach($listeItems as $item){
	echo "Nom :".$item->nom ."<br>  Description : ".$item->liste->titre ."<br><br>";
}


