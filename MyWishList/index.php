<?php

session_start();
/**
 * Import
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \mywishlist\Controleurs as c;
use \mywishlist\Vues as v;

/**
 * Connexion à la BDD
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim;

/**
 * ROUTING
 */

// Page d'une liste
$app->get('/liste/:id',function($id){
    if(isset($_SESSION['id'])) {
        (new c\ControleurCreateur)->getListe($id);
    }
    else {
        (new c\ControleurParticipant)->getListe($id);
    }
})->name("liste");

// Page d'un item
$app->get('/item/:tokenL/:idI',function($tokenL,$idI){
    if(isset($_SESSION['id'])) {
        (new c\ControleurCreateur())->getItem($tokenL, $idI) ;
    }
    else {
        (new c\ControleurParticipant)->getItem($tokenL, $idI);
    }
})->setName('item') ;

// Page d'un item réservé
$app->get('/item/:tokenL/:idI/reserver',function($tokenL,$idI){
    (new c\ControleurParticipant)->reserver($tokenL,$idI); //redirection erreur
})->setName('reserver') ;

$app->post('/validerReservation',function(){
	if(isset($_SESSION['ResItem']) && isset($_SESSION['ResListe'])){
		(new c\ControleurParticipant)->ValiderReservation();
		unset($_SESSION['ResItem']);
		unset($_SESSION['ResListe']);
	}
	else{
		\Slim\Slim::getInstance()->redirectTo('root');
	}
})->setName('validerReservation');

// Page d'un compte
$app->get('/compte/:id', function($id) {
    if(isset($_SESSION['id']))
        (new c\ControleurCreateur())->voirCompte($id) ;
    else
        (new c\ControleurParticipant())->voirCompte($id) ;
})->name('compte') ;

// Page du formulaire de connexion
$app->get('/connexion',function() use ($app){

    if(isset($_SESSION['slim.flash']['errors'])){
        $errors=$_SESSION['slim.flash']['errors'];
    } else {
        $errors=null;
    }
    (new c\ControleurConnexion())->connexion_rendu($errors); //redirection erreur
    unset($_SESSION['slim.flash']);
})->name('connexion') ;

// Page d'un formulaire d'inscription
$app->get('/inscription',function(){
    if(isset($_SESSION['slim.flash']['errors'])){
        $errors=$_SESSION['slim.flash']['errors'];
    } else {
        $errors=null;
    }
   (new c\ControleurInscription)->inscription($errors); //redirection erreur
   unset($_SESSION['slim.flash']);

})->name('inscription') ;

// Page de la création d'une liste
$app->get('/creer-sa-liste',function(){
    (new c\ControleurListe())->render() ; //redirection erreur déjà fait
})->name('creationListe') ;

// Page d'accueil
$app->get('/',function(){
    (new v\VuePageAccueil())->accueil() ;
})->name('root') ;

// Page d'affichage de l'ensemble des listes publiques
$app->get('/listes',function(){
    (new c\ControleurParticipant)->getListes() ;
})->name('listes') ;

// POST du formulaire de connexion
$app->post('/valider_co', function() use($app) {
    (new c\ControleurConnexion)->connect() ;
})->name('valider_co') ;

// POST du formulaire d'inscription
$app->post('/valider_inscription',function(){
	$app = Slim\Slim::getInstance();
    (new c\ControleurInscription)->verifierInscription($app->request->post());
})->name('valider_inscription');

// POST du formulaire de création d'une liste
$app->post('/valider_creation_liste',function(){
    $app = Slim\Slim::getInstance();
    (new c\ControleurListe)->creationListe();
})->name('valider_creaListe');

// Page de déconnexion
$app->get('/deconnexion', function (){
    (new c\ControleurConnexion)->deconnect() ; //redirection erreur
})->name('deconnexion');

// Redirection lorsque la page n'existe pas
$app->notFound( function (){
    (new v\VueErreur)->erreur404();
});

// POST d'ajout d'un message dans le chat d'une liste
$app->post('/ajout_message/:id',function($id){
    (new c\ControleurParticipant)->ajoutMessage($id) ;
})->name("messageListe");

// Page à propos
$app->get('/a_propos', function() {
    (new v\VuePageAccueil())->a_propos() ;
})->name("a_propos");

// Page de suppression d'un item dans une liste
$app->get('/supprimer/:idListe/:idItem', function ($idListe,$idItem){
	(new c\ControleurListe())->supprimerItem($idListe,$idItem);
})->name('supprItem') ;

// Page d'ajout d'un item
$app->get('/liste/:id/item/ajouter', function ($id){
    if(isset($_SESSION['slim.flash']['errors'])){
        $errors=$_SESSION['slim.flash']['errors'];
    } else {
        $errors=null;
    }
    (new c\ControleurListe())->formulaireAjoutItem($id,$errors); //redirection erreur
})->name('ajoutItem') ;

// POST du formulaire d'ajout d'un item dans une liste
$app->post('/liste/:id/item/valider_ajoute_item', function ($id){
    if(isset($_SESSION['id'])) {
        (new c\ControleurListe())->ajouterItem($id);
    }
})->name('valider_ajoutItem') ;

// Pag de modification des informations d'un item
$app->get('/item/:tokenListe/:id/modifier',function ($tokenListe,$id){
	$c=new c\ControleurListe();
	$c->modifierItem($tokenListe,$id);
})->name('modifItem') ;


// Pag de validation des informations d'un item
$app->post('/item/:tokenListe/:id/modifier',function ($tokenListe,$id){
	$c=new c\ControleurListe();
	$c->validerItem($tokenListe,$id);
})->name('valider_modifItem') ;

// Page de mofication des informations d'une liste
$app->get('/liste/modifier/:id' ,function($id){
    if(isset($_SESSION['id'])) {
        (new c\ControleurCreateur)->modifierInformationsListe($id); //redirection erreur
    }
})->name('modifListe') ;

// POST du formulaire de modification des informations d'une liste
$app->post('/valider_modification_liste/:id', function ($id){
    if(isset($_SESSION['id'])) {
        $app = Slim\Slim::getInstance();
        var_dump($id);
        (new c\ControleurListe)->modifierInformations($id);
    }
})->name('valider_modifListe') ;

$app->get("/compteSuppression", function(){
    //$_SESSION['messageErreur'] = 'Cette fonctionnalité ne fonctionne pas pour le moment' ;
    //$app->redirectTo('root') ;
    (new c\ControleurCreateur())->supprimerCompte() ;
})->name('supprCompte') ;

$app->run() ;