<?php
/**
 * Created by PhpStorm.
 * User: Cath
 * Date: 27/12/2017
 * Time: 20:00
 */

namespace mywishlist\Vues;

/**
 * Class VueErreur
 * Gère l'affichage des codes d'erreur
 * @package mywishlist\Vues
 */
class VueErreur
{

    /**
     * Affiche un code d'erreur si l'erreur est un code 404
     */
    public function erreur404() {
        $html = <<<HTML
        <h1>Erreur 404</h1>
        <h2>Oups....</h2>
        <p>La page que vous cherchez est inexistante.</p>
HTML;

        $vue = new VuePageHTML($html) ;
        $vue->showHtml() ;
    }
}