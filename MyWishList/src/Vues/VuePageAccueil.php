<?php

namespace mywishlist\Vues;

use mywishlist\Modeles\Utilisateur;
use Slim\Slim;


/**
 * Class VuePageAccueil
 * Gère la page d'accueil du site
 * @package mywishlist\Vues
 */
class VuePageAccueil
{

    /**
     * Affichage du contenu de la page d'accueil
     */
    public function accueil () {
		$message=null;
		if(isset($_SESSION['messageErreur'])){
			$message=$_SESSION['messageErreur'];
			unset($_SESSION['messageErreur']);
		}
        $content= <<<HTML
	<div style="margin-top:5%;text-align:center;color:forestgreen;">$message</p></div>

HTML;
		if(!isset($_SESSION['id'])){
            $content.= $this->createursPubliques() ;
        } else {
            $content.= $this->createursConnectes();
        }

        $html = new VuePageHTML($content);

        $html->showHTML();
    }

    /**
     * Affichage de l'accueil pour un visiteur
     * @return string
     */
    private function createursPubliques() {
        $createurs = Utilisateur::createurActif() ;
        $app = Slim::getInstance() ;
        $createursAffiches = array() ;
        $html = <<<HTML

<div class="bodyAccueil">
<div class="main">
    <h1>Vous n'êtes pas encore inscrit ? N'attendez plus, créez vos listes de souhaits publiques comme ces utilisateurs :</h1>
HTML;
        foreach ($createurs as $c) {
            if(!in_array($c, $createursAffiches)) {
                $createursAffiches[] = $c;
                $html .= "<p><a href='{$app->urlFor('compte', array('id'=>$c->id))}'>" . $c->nom . "</a><p>";
            }
        }
        $html .= "</div></div>";

        return $html ;
    }

    /**
     * Affiche la section à propos du site
     */
    public function a_propos() {
        $content = <<<HTML
<div class="bodyAccueil">
    <div class="main">
        <p>Ce projet a été réalisé par Océane BERTHE, Titouan BOUDART, Chloé MASONI et Guerric REMOIVILLE dans le cadre des cours de PHP avec M.GUNEGO.</p>
        <p>Le site est également disponible sur Webetu avec cette url : <a href="https://webetu.iutnc.univ-lorraine.fr/www/boudart3u/MyWishList/">https://webetu.iutnc.univ-lorraine.fr/www/boudart3u/MyWishList/</a></p>
    </div>
</div>
HTML;

        $vue = new VuePageHTML($content) ;
        $vue->showHTML() ;
    }

    /**
     * Affichage de l'accueil pour un utilisateur connecte
     * @return string
     */
    private function createursConnectes() {
        $html = <<<HTML

<div class="bodyAccueil">
<div class="main">
    <h1 style="text-align: center">Bienvenue sur MyWishList !</h1>
    <h2 style="text-align: center">Gérez vos listes selon vos envies</h2>
    <p>Nouvelle fonctionnalité : vous pouvez désormais consulter la liste des utilisateurs qui ont au moins une liste publique active. N'hésitez pas à consulter leurs comptes ! </p>
HTML;
        $createursAffiches = array() ;
        $createurs = Utilisateur::createurActif() ;
        $app = Slim::getInstance() ;
        foreach ($createurs as $c) {
            if(!in_array($c, $createursAffiches)) {
                $createursAffiches[] = $c;
                $html .= "<p><a href='{$app->urlFor('compte', array('id'=>$c->id))}'>" . $c->nom . "</a><p>";
            }
        }
        $html .= "</div></div>";

        return $html ;
    }
}