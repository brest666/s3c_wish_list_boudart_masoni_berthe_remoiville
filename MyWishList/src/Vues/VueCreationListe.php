<?php
/**
 * Vue de la création d'une liste
 */

namespace mywishlist\Vues;

use Slim\Slim;

/**
 * Class VueCreationListe
 * Gère le formulaire de création de liste
 * @package mywishlist\Vues
 */
class VueCreationListe {

    /**
     * @var null
     */
    protected $error=null;

    /**
     * VueCreationListe constructor.
     * @param $error : affiche une erreur
     */
    public function __construct($error)
    {
        $this->error=$error;
    }

    /**
     * Rendu formulaire de création de liste
     */
    public function create () {
        $app = Slim::getInstance() ;
        $datecourante= (new \DateTime())->format('Y-m-d' );
        $content= <<<HTML
<div class="bodyCreationListe">
    <h1>Création d'une liste</h1>
    <div class="main">
        <div style="text-align:center;color:red;">$this->error</p></div>
        <form class="formulaire" action="{$app->urlFor('valider_creaListe')}" method="post">
        <div id="section">
        <label id="identifiants" for="nomListe">Nom de la liste*</label>
            <input type="text" name="nomListe" required autofocus/>
         </div>   
         
            <div id="section">
        <label id="identifiant" for="descriptionListe">Description de votre liste*</label>    
            <textarea  type="text" name ="descriptionListe" maxlength="500" id="descrListe" required></textarea> 
            </div>
            
            <div id="section">
        <label class="identifiant" for="expirationListe">Date d'expiration*</label>    
            <input id="expiration" type="date" name="expirationListe" required min={$datecourante} max="2099-12-31"/>
            </div>
            
            <div id="section">
        <label id="identifiants"  for="ValeurPublic">Rendre cette liste publique :</label>
        <SELECT id="identifiants" name="ValeurPublic">
         <option value="1" selected>Oui</option>
         <option value="0" >Non</option>
        </SELECT>
        </div>
            
            <input type="submit" value="Valider" name="Valider" id="buttonValiderCreation"/>
        </form>
    </div>
</div>
HTML;
        $html = new VuePageHTML($content);

        $html->showHTML();
    }

}

