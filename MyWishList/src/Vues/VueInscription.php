<?php

namespace mywishlist\Vues;
use Slim\Slim;

/**
 * Class VueInscription
 * Gère le formulaire de l'inscription
 * Controleur : ControleurInscription
 * @package mywishlist\Vues
 */
class VueInscription
{
    /**
     * @var null
     */
	protected $errors=null;

    /**
     * VueInscription constructor.
     * @param $errors
     */
	function __construct($errors){
		$this->errors=$errors;
	}

    /**
     * Rendu de la vue
     */
    public function render()
    {

        $app = Slim::getInstance();
        $content = <<<HTML
		<div class="bodyConnexion">
		<h1>Inscription</h1>
		<div class="formulaireInscription">
		<p style="color:red;">$this->errors</p>
        <form action={$app->urlFor('valider_inscription')} method="post">
            <label for="identifiant" class="identifiants">Identifiant</label>
            <input type="text" name="identifiant" required autofocus/> 
			<p style="line-height:1em;"></p>
			<label for="Mail" class="identifiants">Mail</label>
			<input type="email" name ="Mail" required/>
            <p style="line-height:1em;"></p>
            <label for="MotDePasse" class="identifiants">Mot de passe</label>
            <input type="password" name ="MotDePasse" required/>
			 <p style="line-height:1em;"></p>
			<label for="MotDePasseValidation" class="identifiants">Validation Mot de passe</label>
			<input type="password" name ="MotDePasseValidation" required/>
            <p style="line-height:1em;"></p>
				<input type="submit" value="Valider" name="Valider" id="buttonValiderInscription"/>
			</form>
		</div>
	</div>
HTML;
	$html = new VuePageHTML($content);
	$html->showHTML();
	}
}

