<?php

namespace mywishlist\Vues;

use mywishlist\Modeles\Message;
use mywishlist\Modeles\Participant;
use mywishlist\Modeles\Item;
use mywishlist\Modeles\Utilisateur;
use Slim\Slim;

/**
 * Class VueListe
 * Gère la vue des listes
 * @package mywishlist\Vues
 */
class VueListe {

    /**
     * @var $affichage :
     * @var $createur :
     * @var $expiree :
     * @var AFF_ALL_LISTS :
     * @var AFF_ONE_ITEM :
     * @var AFF_ITEMS_OF_LIST :
     * @var AFF_RESERVATION :
     * @var AFF_LIST_CREATEUR :
     * @var AFF_ITEM_CREATEUR :
     */
    protected $affichage ;
    protected $createur ; //permet de savoir si l'affichage doit se faire pour le créateur de la liste ou non
    protected $expiree ; //permet de savoir si la liste est expiree ou non
    const AFF_ALL_LISTS = 1 ;
    const AFF_ONE_ITEM = 2 ;
    const AFF_ITEMS_OF_LIST = 3 ;
    const AFF_RESERVATION = 4 ;
    const AFF_LIST_CREATEUR = 5 ;
    const AFF_ITEM_CREATEUR = 6 ;
    protected $id_liste_actuelle=null;
    protected $message=null;
    /**
     * VueListe constructor.
     * @param $array
     */
    public function __construct($array, $message = null) {
        $this->affichage = $array ;
        $this->createur = false ;
        $this->expiree = false ;
        $this->message=$message;
    }

    /**
     * Affiche toutes les listes (titre+descr)
     * @return string
     */
    private function listsView() {
        $app = Slim::getInstance() ;
        //href={$app->urlFor('liste/'.$token)}
        $html = "<section>" ;
        $html.= "<h1> Voici toutes les listes de souhaits publiques présentes sur le site </h1>" ;

        foreach ($this->affichage as $val) {
            $token = $val->token ;
            $html.= "<p class='listes'> <a href={$app->urlFor('liste',array('id' => $token))}>$val->titre </a> : " . $val->description . "</p>" ;
        }
        $html.= "</section>" ;
        return $html ;
    }

    /**
     * Calcule si une liste est perimee ou non et met à jour l'attribut expiree
     * @param $dateListe date de la liste à prendre en compte
     */
    public function etreExpiree($dateListe) {
        $dateCourante = new \DateTime() ;
        $dateListe = new \DateTime($dateListe) ;
        if($dateCourante>$dateListe)
            $this->expiree = true ;
        else
            $this->expiree = false ;
    }
    /**
     * Retourne tous les items d'une liste
     * @return string
     */
    private function itemsOfList() {
        $app = Slim::getInstance() ;
        $expiration = new \DateTime($this->affichage->expiration) ;
        $expiration = $expiration->format('d/m/Y') ;
        $html= "<h1>". $this->affichage->titre . " (expire le " . $expiration . ")</h1>";
        $html.= "<section>" ;
        $items = $this->affichage->items ;

        //si c'est le createur qui consulte la liste les messages ne doivent s'afficher qu'à expiration de la liste
        if(!$this->createur || ($this->createur && $this->expiree)) {
            $html .= $this->messageListe($this->affichage->id) . "<section class='sectionListe'>";
        }
        else {
            $token=$this->affichage->token;
            $html .= <<<HTML
<section class='sectionSansMessage'>
    <a href={$app->urlFor("modifListe",array('id' => $token))}>Modifier la liste </a>
    <br>
    <a href="{$app->urlFor('ajoutItem',array('id' => $token))}">Ajouter un nouvel item</a>
HTML;

        }

        foreach ($items as $val) {
            $html.= $this->item($val) ;
        }
        $html.= "</section>" ;
        return $html ;
    }

    /**
     * Permet d'afficher un item de la liste
     * @param null $i Item à afficher. Si $i=null alors l'item est contenu dans $this->affichage et on attend tous les détails de l'item (prix, participants...)
     * @return string Code html de l'item à afficher
     */
    private function item($i=null) {
        $app = Slim::getInstance() ;
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9) ;
        $descriptionDetaillee = false ;
        $classItem = "" ;

        if(!isset($i)) {
            $i = $this->affichage;
            $descriptionDetaillee = true ;
            $classItem = "<section class='sectionSansMessage'>" ;
        }

        $url=array("tokenL"=>$i->liste->token, "idI"=>$i->id) ;
        if($this->createur && !$this->expiree)
            $array = $this->participantsAffichageCreateur($i) ;
        else
            $array = $this->participants($descriptionDetaillee, $i) ;
        $particip = $array['particip'] ;
        $class = $array['class'] ;
        $messages = "";

        if(isset($array['nombreMessages'])) {
            $nbMessages = $array['nombreMessages'];

            if ($nbMessages > 0)
                $messages = $this->messageItem($i);
        }

        $details = "" ;
        $lienItem = "<b> $i->nom </b>" ;
        if($descriptionDetaillee) {
            $lien="" ;
            if($i->url!="") {
                $lien = "<li>Lien : <a href='{$i->url}'>$i->url</a> </li>" ;
            }
            $tarif = "<li> Prix : $i->tarif € </li>";

            $details = <<<HTML
            <p><u> Détails : </u> </p>
            <ul>
            $tarif 
            $lien
            </ul>
HTML;
        }
        else {
            $lienItem = " <a href={$app->urlFor('item', $url)}><b> $i->nom </b></a>";
        }

        if(isset($i->img)) {
            $img = $i->img ;
        }
        else
            $img = 'gift.jpg' ;
        $html = <<<HTML
        <h1>$i->nomListe</h1>
        $classItem
        <div class="item $class">
            <img src={$racine}img/{$img}>
            <p> $lienItem : $i->descr </p>
            $details            
            $particip
            $messages
        </div>
HTML;

        return $html ;
    }

    /**
     * Permet de connaitre les participants d'un item
     * @param $descriptionDetaillee pour savoir si c'est destiné à un affichage détaillé (nom des participants) ou non
     * @param $i item concerné
     */
    private function participants($descriptionDetaillee, $i) {
        $app = Slim::getInstance() ;
        $url=array("tokenL"=>$i->liste->token, "idI"=>$i->id) ;
        $class = "";
        $participants = $i->participants();
        $nbParticipants = 0 ;

        //si c'est reserve => itemReserve
        //si c'est pas reserve => si participant vide 'pas de participant. Sinon 'en partie reserve'
        if($participants['reserve']) {
            $particip = "Item réservé";
            $class = "itemReserve";
            if ($descriptionDetaillee) {
                $particip .= " par ";
                foreach ($participants['participants'] as $p) {
                    $particip .= $p->participant . ", ";
                }
                $particip = substr($particip, 0, strlen($particip) - 2) . ".";
                //seul le createur peut voir les messages une fois la loiste expiree
                if($this->createur)
                    $nbParticipants = count($participants) ;
            }
        }
        else {
            //On ajoute les liens uniquement si la liste n'est pas expirée
            if(!$this->expiree) {
                if (count($participants['participants']) == 0) {
                    $particip = "Pas encore réservé. <a href={$app->urlFor('reserver', $url)}>Je le réserve</a>";
                } else {
                    $particip = "Item en partie réservé";
                    if ($descriptionDetaillee) {
                        $particip .= " par ";
                        foreach ($participants['participants'] as $p) {
                            $particip .= $p->participant . ", ";
                        }
                        $particip = substr($particip, 0, strlen($particip) - 2) . ".";
                        $particip .= " <a href=#>Je souhaite participer</a>";
                    } else {
                        $particip .= ". <a href={$app->urlFor('reserver', $url)}>Je souhaite participer</a>";
                    }
                }
            }
            else {
                $particip = "Item non réservé" ;
            }
        }

        return array('class'=>$class, 'particip'=>$particip, 'nombreMessages'=>$nbParticipants) ;
    }

    /**
     * Permet de connaitre les participants d'un item
     * @param $descriptionDetaillee pour savoir si c'est destiné à un affichage détaillé (nom des participants) ou non
     * @param $i item concerné
     */
    private function participantsAffichageCreateur($i)
    {
        $app = Slim::getInstance();
        $url = array("tokenL" => $i->liste->token, "idI" => $i->id);
        $class = "";
        $participants = $i->participants();

        //si c'est reserve => itemReserve
        //si c'est pas reserve => si participant vide 'pas de participant. Sinon 'en partie reserve'
        if (count($participants['participants'])>0) {
            $particip = "<i>Item réservé</i>";
            $class = "itemReserve";
        } else {
			$paramSuppr=array("idItem"=>$i->id,"idListe"=>$i->idListe);
            $particip = <<<HTML
            <i>Item pas encore réservé.</i>
            <a href="{$app->urlFor('modifItem',array('tokenListe'=>$i->liste->token,'id'=>$i->id))}">Modifier</a>
            <a href="{$app->urlFor('supprItem',$paramSuppr)}">Supprimer</a>
HTML;

        }

        return array('particip'=>$particip, 'class'=>$class) ;
    }

    /**
     * Permet d'afficher les messages correspondant à un item
     * @param $i item en question
     */
    private function messageItem($i) {
        $participants = $i->participants();
        $nbMessages = 0 ;

        if(count($participants['participants'])==1)
            $html = "<p>Message laissé :</p><ul>" ;
        else
            $html = "<p>Messages laissés :</p><ul>" ;

        foreach ($participants['participants'] as $p) {
            if($p->texte!=null) {
                $nbMessages++ ;
                $nom = $p->participant;
                $message = $p->texte;
                $html .= <<<HTML
    <li><u>$nom :</u> $message</li>       
HTML;
            }
        }
        if($nbMessages!=0)
            $html.= "</ul>" ;
        else
            $html = "" ;

        return $html ;
    }

    /**
     * Gère l'affichage des réservations
     * @return string
     */
    private function reservation(){
		$nom=Utilisateur::nomSession();
		$tarif=<<<HTML
		<div id="section">
			<label id="identifiants" for="tarif" id="tarif">Tarif</label>
            <input type="number" name ="tarif" required/>
            </div>
HTML;
		if(Item::find($_SESSION['ResItem'])->cagnotte==false){
			$tarif=null;
		}
		if($nom!=null)$nom="value=".$nom;
        $app = Slim::getInstance();
        $content = <<<HTML
<div class="bodyCreationListe">
    <h1>Réservation</h1>
    <div class="main" style="width:30%; height: 40%;">
        <form class="formulaire" action={$app->urlFor('validerReservation')} method="post">
        
        <div id="section">
            <label id="identifiants" for="nom" id="Nom">Nom</label>
            <input type="text" $nom name="identifiant" required autofocus/>
            </div>
            
            <div id="section">
            <label id="identifiants" for="message" id="message">Message</label>
            <input type="text" name ="message"/>
            </div>
            $tarif
            <input type="submit" value="Réserver" name="Réserver" id="buttonValiderCreation"/>
        </form>
    </div>
</div>
HTML;
        return $content;
    }

    /**
     * Gère l'affichage des messages
     * @param $id
     * @return string
     */
    private function messageListe($id) {
        $app = Slim::getInstance() ;
        $html = "<section class='sectionMessage'>" ;
        $p = new Participant() ;
        $messages = $p->messagesListe($id) ;

        foreach ($messages as $m) {
            $dateFormat = new \DateTime($m->date) ;
            $dateFormat = $dateFormat->format('d/m/Y' ) ;
            $html.= <<<HTML
<div class="divListeMessage">
    <p><b>$m->participant : </b></p> 
    <p>$m->texte</p> 
    <p><i>Le $dateFormat</i></p>
</div>
HTML;
        }

        if(count($messages)==0 && !$this->createur) {
            $html.="<div style='text-align:center'>Souhaitez-vous ajouter un nouveau message global à la liste ?</div>" ;
        }

        if(!$this->createur) {
           $nom=Utilisateur::nomSession();
			if($nom!=null)$nom= $nom = "value=".Utilisateur::nomSession();

            $html .= <<<HTML
<form action={$app->urlFor('messageListe', array('id' => $id))} method="post" class="formulaireMessage">
        <div class="nomChamp">
            <label for="participant">Nom</label>
        </div>
        <div>
            <input type="text" $nom name="participant" required autofocus/>
        </div>
        <div class="nomChamp">
            <label for="Message">Message</label>
        </div>
        <div>
            <input type="text" name ="texte" required/>
        </div>
        <div class="valider">
            <input type="submit" value="Valider" name="Valider"/>
        </div>
</form>
</section>
HTML;
        }
        else
            $html.= "</section>" ;

        return $html ;
    }

    /**
     * Affichage page de modification d'une liste
     */
    public function modification($liste){
        $app=Slim::getInstance();
        $datecourante= (new \DateTime())->format('Y-m-d' );
        $content= <<<HTML
        <div class="bodyCreationListe">
        <h1>Modifier les informations de la liste</h1>
		<div class="main">
        <form class="formulaire" action="{$app->urlFor('valider_modifListe', array('id' => $liste->token))}" method="post">
        <div id="section">
        <label for="nomListe" id="identifiants">Nom de la liste (Ne peut être modifié)</label>
            <input style="background-color:#b6b6b6 ;" value={$liste->titre} type="text"  readonly name="nomListe" required autofocus/>
        </div>
         
         <div id="section">
        <label for="descriptionListe" id="identifiants">Modifier la description de votre liste</label>    
            <textarea type="text" id="descrListe" name ="descriptionListe" maxlength="500" id="descrListe" required>{$liste->description}</textarea> 
        </div>   
            <div id="section">
        <label for="expirationListe" id="identifiants">Modifier la date d'expiration</label>    
            <input value={$liste->expiration} id="expiration" type="date" name="expirationListe" required min={$datecourante} max="2099-12-31"/>
            </div>
            <div id="section">
        <label for="ValeurPublic" id="identifiants">Voulez-vous que cette liste soit publique ?</label>
        <SELECT name="ValeurPublic">
         <option value="1">Oui</option>
         <option value="0">Non</option>
        </SELECT>
        </div>
            <!--Penser à demander si la liste doit être pubique ou privée-->
            
            <input type="submit" value="Valider" name="Valider" id="buttonValiderCreation"/>
        </form>
    </div>
</div>
HTML;

        $vue = new VuePageHTML($content) ;
        $vue->showHTML() ;
    }

    /**
     * Affiche un nouvel item
     * @param $token
     */
    public function ajouterUnItem($token){
        $app=Slim::getInstance();
        $content=<<<HTML
        <div class="bodyCreationListe">
<h1>Ajouter un item</h1>

    <div class="main">
    <p style="color:red">{$this->message}</p>
        <form class="formulaire" action="{$app->urlFor('valider_ajoutItem',array('id' => $token))}" method="post" enctype="multipart/form-data">
        
        <div id="section">
        <label id="identifiants" for="nomItem">Nom: </label>
            <input type="text" name="nomItem" required autofocus/>
            </div>
            <div id="section">
        <label id="identifiants" for="descriptionItem">Description</label>    
            <textarea type="text" name ="descriptionItem" maxlength="500" id="descriptionItem" required></textarea> 
            </div>
            <div id="section">
            
        <label id="identifiants" for="imgItem">Image: </label>
            <input type="file" name="imgItem" />
            </div>
            <div id="section">

        <div id="section">
        <label id="identifiants" for="url">Lien vers l'article en ligne</label>    
            <input id="url" type="text" name="url" />
            </div>
            <div id="section">
        <label id="identifiants" for="tarif">Tarif : </label>    
            <input id="tarif" type="text" name="tarif" required/> 
               </div>
               <div id="section">
        <label id="identifiants" for="cagnotte">Cagnotte :</label>       
        <Select name="cagnotte">
        <option value="0" selected>Non</option>
        <option value="1">Oui</option>
</Select>       
</div>
            
            <input type="submit" value="Valider" name="Valider" id="buttonValiderCreation"/>
        </form>
    </div>
</div>
HTML;

        $vue = new VuePageHTML($content) ;
        $vue->showHTML() ;
    }

	public function modifierUnItem($token,$id){
		$item=Item::find($id);
		$zselected=null;
		$uselected=null;
		if($item->cagnotte==1)$uselected="selected";
		if($item->cagnotte==0)$zselected="selected";
        $app=Slim::getInstance();
        $content=<<<HTML
        <div class="bodyCreationListe">
<h1>Modifier l'item</h1>

    <div class="main">
    <p style="color:red">{$this->message}</p>
        <form class="formulaire" action="{$app->urlFor('valider_modifItem',array('tokenListe'=>$token,'id' => $id))}" method="post" enctype="multipart/form-data">
        
        <div id="section">
        <label id="identifiants" for="nomItem">Nom: </label>
            <input type="text" value="$item->nom" name="nomItem" required autofocus/>
            </div>
            <div id="section">
        <label id="identifiants" for="descriptionItem">Description</label>    
            <textarea type="text" name="descriptionItem" maxlength="500" id="descriptionItem" required>$item->descr</textarea> 
            </div>
            <div id="section">
            
        <label id="identifiants" for="imgItem">Image: </label>
            <input type="file" name="imgItem" />
            </div>
            <div id="section">
            
        <label id="identifiants" for="url">Lien vers l'article en ligne</label>    
            <input id="url" type="text" value="$item->url" name="url" />
            </div>
            <div id="section">
        <label id="identifiants" for="tarif">Tarif : </label>    
            <input id="tarif" type="text" value="$item->tarif" name="tarif" required/> 
               </div>
               <div id="section">
        <label id="identifiants" for="cagnotte">Cagnotte :</label>       
        <Select name="cagnotte">
        <option value="0" $zselected>Non</option>
        <option value="1" $uselected>Oui</option>
</Select>  
     </div>
            
            <input type="submit" value="Valider" name="Valider" id="buttonValiderCreation"/>
        </form>
    </div>
</div>
HTML;

        $vue = new VuePageHTML($content) ;
        $vue->showHTML() ;
    }

    /**
     * Permet d'appeler la bonne fonction d'affichage
     * @param $i Numéro de la fonction à afficher
     * @return string
     */
    public function render($i) {
        $this->createur = false ;

        switch($i) {
            case VueListe::AFF_ALL_LISTS : $html = $this->listsView() ;
                break ;
            case VueListe::AFF_ITEMS_OF_LIST : $html = $this->itemsOfList() ;
                break ;
            case VueListe::AFF_ONE_ITEM : $html = $this->item() ;
                break ;
            case VueListe::AFF_RESERVATION : $html = $this->reservation() ;
                break ;
            case VueListe::AFF_LIST_CREATEUR :
                $this->createur = true ;
                $html = $this->itemsOfList() ;
                break ;
            case VueListe::AFF_ITEM_CREATEUR :
                $this->createur = true ;
                $html = $this->item() ;
                break ;
        }
        $vue = new VuePageHTML($html) ;
        $vue->showHTML() ;
    }

    /**
     * Définie la liste actuelle
     * @param $id
     */
    public function setListeActuelle($id){
        $this->id_liste_actuelle=$id;
    }
}