<?php

namespace mywishlist\Vues;
use mywishlist\Modeles\Item;
use mywishlist\Modeles\Liste;
use mywishlist\Modeles\Participant;
use Slim\Slim;

/**
 * Class VueCompte
 * Gère l'affichage de la vue d'un compte
 * @package mywishlist\Vues
 */
class VueCompte
{

    /**
     * @var AFF_COMPTE_VUE_PROPRIETAIRE : Affichage d'un compte lorsqu'il s'agit du propriétaire
     * @var AFF_COMPTE_VUE_EXTERIEURE : Affichage d'un compte lorsqu'il s'agit d'un ivisteur
     * @var $content :
     */
    const AFF_COMPTE_VUE_PROPRIETAIRE = 0 ;
    const AFF_COMPTE_VUE_EXTERIEURE = 1 ;
    protected $content ;

    /**
     * VueCompte constructor.
     * @param $c
     */
    public function __construct($c)
    {
        $this->content = $c ;
    }

    /**
     * Gère l'affichage d'un compte lorsqu'il s'agi de son propriétaire
     * @return string
     */
    private function monCompte()
    {
        $app = Slim::getInstance();
        $nom = $this->content->nom ;
        $content = <<<HTML
<div class="bodyCompte">        
    <h1>Compte</h1>
        <div id="informations">
            <p>Bonjour $nom. </p>
HTML;

        $listes = Liste::getUserLists($_SESSION['id']) ;
        if(count($listes)==0) {
            $content.= "<p>Vous n'avez encore créé aucune liste. <a href='{$app->urlFor('creationListe')}'>Commencez maintenant !</a>" ;
        }
        else {
            $content.= "<p>Voici les listes que vous avez créées :</p><ul>" ;
            foreach ($listes as $l) {
                $nom = $l->titre ;
                $id = $l->token ;
                if($l->public)
                    $type = "publique" ;
                else
                    $type = "privée" ;

                $dateCourante = new \DateTime() ;
                $dateListe = new \DateTime($l->expiration) ;
                if($dateCourante>$dateListe)
                    $etat = "expirée" ;
                else
                    $etat = "en cours" ;

                $content.= "<li><a href='{$app->urlFor('liste', array('id'=>$id))}'>$nom</a> <i>($type, $etat)</i>" ;
            }
            $content.= "</ul><p><a href='{$app->urlFor('creationListe')}'>Créer une nouvelle liste</a></p>" ;
        }

        $participations = Participant::participations($_SESSION['id']) ;
        if(count($participations)!=0) {
            $content.= "<p>Vous avez déjà réservé tous ces items :</p><ul>" ;
            foreach ($participations as $p) {
                $nomItem = Item::getNom($p->idItem)->nom ;
                $liste = Liste::getList($p->idListe) ;
                $nomListe = $liste->titre ;
                $prop = $liste->proprietaire() ;
                $proprietaire = $prop->nom ;
                $arrayItem = array('tokenL'=>$liste->token, 'idI'=>$p->idItem) ;
                $arrayList = array('id'=>$liste->token) ;
                $arrayProprio = array('id'=>$prop->id) ;
                $prix = $p->tarif ;

                $content.= <<<HTML
<li><a href="{$app->urlFor('item', $arrayItem)}">$nomItem</a> 
dans la liste 
<a href="{$app->urlFor('liste', $arrayList)}">$nomListe</a> de 
<a href="{$app->urlFor('compte', $arrayProprio)}">$proprietaire</a> ($prix €)</li>
HTML;

            }
            $content.= "</ul> " ;
        }
        $content.= <<<HTML
        <div><a href='{$app->urlFor('supprCompte')}'>Supprimer le compte</a></div>
    </div>
</div>
HTML;

        return $content ;
    }

    /**
     * Gère l'affichage d'un compte lorsqu'il s'agit d'un visiteur
     * @return string
     */
    private function compteExterieur() {
        $app = Slim::getInstance() ;
        $nom = $this->content->nom ;
        $content = <<<HTML
<div class="bodyCompte">        
    <h1>Compte</h1>
        <div id="informations">
            <p>Voici les listes publiques publiées par $nom :</p>
            <ul>
HTML;

        $listes = Liste::getUserLists($this->content->id) ;
        foreach ($listes as $i=>$l) {
            $nomListe = $l->titre ;
            $id = $l->token ;

            $dateCourante = new \DateTime() ;
            $dateListe = new \DateTime($l->expiration) ;
            if($dateCourante<=$dateListe && $l->public) {
                $content .= "<li><a href='{$app->urlFor('liste', array('id'=>$id))}'>$nomListe</a></i>";
            }
            else
                unset($listes[$i]) ;
        }
        if(count($listes)==0) {
            $content = <<<HTML
<div class="bodyCompte">        
    <h1>Compte</h1>
        <div id="informations">
            <p>$nom n'a édité aucune liste publique</p>
HTML;
        }

            $content.= <<<HTML
            </ul>
    </div>
</div>
HTML;

            return $content ;
    }

    /**
     * Permet d'appeler la bonne fonction d'affichage
     * @param $i Numéro de la fonction à afficher
     * @return string
     */
    public function render($i) {
        $html = "";
        switch($i) {
            case self::AFF_COMPTE_VUE_EXTERIEURE : $html = $this->compteExterieur() ;
                break ;
            case self::AFF_COMPTE_VUE_PROPRIETAIRE : $html = $this->monCompte() ;
            break ;
        }
        $vue = new VuePageHTML($html) ;
        $vue->showHTML() ;
    }
}