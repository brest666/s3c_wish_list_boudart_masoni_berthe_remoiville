<?php
/**
 * Created by PhpStorm.
 * User: berthe18u
 * Date: 12/12/2017
 * Time: 09:13
 */
namespace mywishlist\Vues;

use Slim\Slim;

/**
 * Class VuePageHTML
 * Créer une page HTML
 * @package mywishlist\Vues
 */
class VuePageHTML {

    /**
     * @var contenu de la page HTML
     */
    private $content ;

    /**
     * VuePageHTML constructor.
     * @param $c
     */
    public function __construct($c) {
        $this->content = $c ;
    }

    /**
     * Code HTML du header
     * @return string
     */
    private function header() {
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9) ;
        $racine.="/style.css";
        return <<<HTML
        <html >
        <head>
            <title> MyWishList</title>
            <link rel="stylesheet" href={$racine}>
            <!--<link type="text/css" rel="stylesheet" href="../../materialize/css/materialize.min.css"  media="screen,projection"/>-->
        </head>
        <body>
HTML;
    }

    /**
     * Code HTML de la barre de navigation / Menu supérieur
     * @return string
     */
    private function navbar(){
        $app=Slim::getInstance();

        if(isset($_SESSION['id'])){
            return <<<HTML
<div class="navbar">
        <h1>My wish list</h1>
        <ul>
             <li><a href={$app->urlFor('deconnexion')}>Deconnexion</a></li>
             <li><a href={$app->urlFor('compte', array('id'=>$_SESSION['id']))}>Mon compte</a></li>
             <li><a href={$app->urlFor('listes')}>Listes publiques</a></li>
             <li><a href={$app->urlFor('a_propos')}>A Propos</a></li>
             <li><a href={$app->urlFor('root')}>Accueil</a></li>
        </ul>
    </div>
HTML;
        }else {
            return <<<HTML
<div class="navbar">
        <h1>My wish list</h1>
        <ul>
             <li><a href={$app->urlFor('inscription')}>Inscription</a></li>
             <li><a href={$app->urlFor('connexion')}>Connexion</a></li>
             <li><a href={$app->urlFor('listes')}>Listes publiques</a></li>
             <li><a href={$app->urlFor('a_propos')}>A Propos</a></li>
             <li><a href={$app->urlFor('root')}>Accueil</a></li>  
        </ul>
    </div>
HTML;
        }
    }

    /**
     * Code HTML du footer
     * @return string
     */
    private function footer() {
        return <<<HTML
        </body>
        </html>
HTML;

    }

    /**
     * Affiche le contenu d'une page HTML
     */
    public function showHTML() {
        echo $this->header().$this->navbar().$this->content.$this->footer() ;
    }
}