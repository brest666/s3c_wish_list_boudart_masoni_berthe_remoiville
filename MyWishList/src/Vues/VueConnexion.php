<?php

namespace mywishlist\Vues;
use Slim\Slim;

/**
 * Class VueConnexion
 * Vue du formulaire de connexion
 * Gérée par ControleurConnexion
 * @package mywishlist\Vues
 */
class VueConnexion
{
    /**
     * @var null
     */
    protected $message=null;

    /**
     * VueConnexion constructor.
     * @param $errors
     */
    function __construct($errors){
        $this->message=$errors;
    }


    /**
     * Rendu de la vue
     */
    public function render()
    {
        $app = Slim::getInstance();
        $content = <<<HTML
    
<div class="bodyConnexion">
    <h1>Connexion</h1>
    <div style="text-align:center;color:red;">$this->message</p></div>
    <div class="formulaireConnexion">
		<p style="color:red;">$this->message</p>
        <form action={$app->urlFor('valider_co')} method="post">
            <label for="identifiant" id="identifiants">Identifiant</label>
            <input type="text" name="identifiant" required autofocus/> 
            <p style="line-height:1em;"></p>
            <label for="MotDePasse" id="identifiants">Mot de passe</label>
            <input type="password" name ="password" required/>
            <p></p>
            <input type="submit" value="Valider" name="Valider" id="buttonValiderConnexion"/>
        </form>
    </div>
</div>
HTML;

        $html = new VuePageHTML($content);

        $html->showHTML();
    }

}