<?php
namespace mywishlist\Controleurs;
use mywishlist\Vues\VueInscription;
use mywishlist\Modeles\Authentification;
use mywishlist\Modeles\Utilisateur;
use Slim\Slim;

/**
 * Class ControleurInscription
 * Gère le formulaire d'inscription
 * Vue gérée : VueInscription
 * @package mywishlist\Controleurs
 */
class ControleurInscription {

    /**
     * Fonction appelée pour le rooting
     */
	function inscription($error){
	   $app=Slim::getInstance();
	    if(!isset($_SESSION['id'])) {
            $v = new VueInscription($error);
            $v->render();
        }
        else {
            $_SESSION['messageErreur'] = "Vous êtes déjà connecté" ;
	        $app->redirectTo('root') ;
        }
	}

    /**
     * Récupère les informations du formulaire et les traite
     * @param $array
     */
	static function verifierInscription($array){
	    $errors=null;
		$baseid=Utilisateur::where('nom','=',$array['identifiant'])->first();
		$baseemail=Utilisateur::where('mail',"=",$array['Mail'])->first();
		if($baseemail!=null){
			$errors=$errors."cet email est déjà utilisé<br>";
		}
		if(isset($baseid->nom)){
			$baseid=$baseid->nom;
		}
		$basemail=Utilisateur::where('mail','=',$array['Mail'])->first();
		if(isset($baseid->mail)){
			$basemail=$basemail->mail;
		}
		if(!filter_var($array['Mail'], FILTER_VALIDATE_EMAIL)){
			$errors=$errors."Email invalide <br>";
		}
		if($array['MotDePasse']!=$array['MotDePasseValidation']){
			$errors=$errors."Le mot de passe et la validation sont différents<br>";
		}
		if(!preg_match('/^[a-zA-Z0-9]{5,12}$/', $array['identifiant'])){
			$errors=$errors."L'username ne doit contenir que des caractères alphanumériques et contenir entre 5 et 12 caractères<br>";
		}
		
		if($baseid==$array['identifiant']){
			$errors=$errors."L'username est déjà pris<br>";
		}
		if($basemail==$array['Mail']){
			$errors=$errors."Cette addresse e-mail est déjà associée à un autre compte<br>";
		}
		if(strlen($array['MotDePasse'])<8){
			$errors=$errors.'Votre mot de passe doit contenir au moins 8 caractères<br>';
		}
		if($errors==null){
			Authentification::createUser($array['identifiant'],$array['MotDePasse'],$array['Mail']);
			//$_SESSION['messageErreur']="<p>Inscription réussie</p>";
            $app = Slim::getInstance();
            $app->flash('success', "<p>Inscription réussie</p>");
			$app->redirectTo('connexion');
		}
		else{
		    $app = Slim::getInstance();
		    $app->flash('errors',$errors);
            //$_SESSION['messageErreur']="Inscription réussie !";
			$app->redirectTo('inscription');
		}
	}
	
}