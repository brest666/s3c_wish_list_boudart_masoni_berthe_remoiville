<?php

namespace mywishlist\Controleurs;

use mywishlist\Vues\VuePageAccueil;
use Slim\Slim;
use mywishlist\Modeles\Authentification;
use mywishlist\Vues\VueConnexion;


/**
 * Class ControleurConnexion
 * Gère la connexion de l'utilisateur
 * Vue associée : VueConnexion
 * @package mywishlist\Controleurs
 */
class ControleurConnexion
{
    /**
     * Affiche le rendu de la vue connexion
     */
    public function connexion_rendu($error = null){
        if(!isset($_SESSION['id'])) {
            $v = new VueConnexion($error);
            $v->render();
        }
        else {
            $_SESSION['messageErreur'] = "Vous êtes déjà connecté" ;
            $app = Slim::getInstance() ;
            $app->redirectTo('root');
        }
    }

    /**
     * Récupère les données du formulaire de connexion et renvoie vers vers Authentification
     */
    public function connect(){

        $app = Slim::getInstance();

        $password = $_POST['password'];
        $userName = $_POST['identifiant'];
        $connecte=Authentification::authenticate($userName,$password);
        if($connecte){
            $app->response->redirect('root');
        }
		
    }

    /**
     * Déconnecte la session courante
     */
    public function deconnect(){
        $etreConnecte=$this->etreUnUtilisateurIdentifie();
        if($etreConnecte) {
            unset($_SESSION['id']);
            unset($_SESSION['level']);
            Slim::getInstance()->redirectTo("root");
        }
        else {
            $app = Slim::getInstance() ;
            $app->redirectTo('root') ;
        }
    }

    /**
     * Vérifier que l'utilisateur est bien connecté
     * @return bool
     */
    public function etreUnUtilisateurIdentifie(){
        if(isset($_SESSION['id'])) {
            return true;
        } else {
            return false;
        }
    }
}