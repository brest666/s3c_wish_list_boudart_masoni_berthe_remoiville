<?php

namespace mywishlist\Controleurs;

use mywishlist\Modeles\Item;
use mywishlist\Modeles\Liste ;
use mywishlist\Modeles\Participant;
use mywishlist\Vues\VueConnexion;
use mywishlist\Vues\VueCreationListe;
use mywishlist\Vues\VueListe;
use mywishlist\Vues\VuePageHTML;
use mywishlist\Controleurs\ControleurConnexion;
use Slim\Slim;

/**
 * Class ControleurCreationListe
 * @package mywishlist\Controleurs
 */
class ControleurListe
{
    /**
     * Affichage de la vue Creation de liste
     */
    public function render(){
        $etreConnecte=(new ControleurConnexion())->etreUnUtilisateurIdentifie();
        if($etreConnecte) {
            $v = new VueCreationListe(null);
            $v->create();
        }else {
            $error="<p> Veuillez vous connecter pour accéder à la création de liste </p>";
            $app=Slim::getInstance();
            $app->flash('errors', $error);
            $app->redirectTo('connexion');
        }
    }

    /**
     * Récupère les informations des formulaires de liste
     * @return bool
     */
    public function traiter()
    {
        $app = Slim::getInstance();

        $error=null;
        $nompost=$_POST['nomListe'];
        $descpost=$_POST['descriptionListe'];
        $expost=$_POST['expirationListe'];
        $public=$_POST['ValeurPublic'];
        //$liste = new Liste();

        // Vérification des POST
        if (isset($nompost)) {
            $nom = htmlspecialchars($nompost);
        }
        if (isset($descpost)) {

            $description = htmlspecialchars($descpost);
        }
        if (isset($expost)) {
            $expiration = htmlspecialchars($expost);
        }

        // Verification de la validité des informations
        if (!empty($nom) && !empty($description) && !empty($expiration)) {
             return null;
        } else {
            $error.= " une erreur est survenue";
            return $error;
        }
    }

    /**
     * R2cupère les informations du formulaire création d'une liste
     */
    public function creationListe(){
        $app=Slim::getInstance();
        $errors=$this->traiter();
        if($errors == null){
            // Créer la liste
            $validation_creation=Liste::createListe($_POST['nomListe'],$_POST['descriptionListe'],$_POST['expirationListe'],$_POST['ValeurPublic']);

            if($validation_creation){
                $app->response->redirect('listes');
            } else {
                $v = new VueCreationListe($errors);
                $v->create();
            }
        }
    }


    /**
     * Pouvoir modifier les informations de la liste
     */
    public function modifierInformations($token){
        $app=Slim::getInstance();
        $errors=$this->traiter();
        if($errors == null){

            // Créer la liste
            //$validation_creation=Liste::where('token','=',$token)->updated(['description' => $_POST['descriptionListe'], 'expiration' => $_POST['expirationListe'], 'public' => $_POST['ValeurPublic']]);
            $validation_creation=Liste::where('token','=',$token)->update(['description' => $_POST['descriptionListe'], 'expiration' => $_POST['expirationListe'], 'public' => $_POST['ValeurPublic']]);
            var_dump($validation_creation);
            if($validation_creation){
                $app->redirectTo('liste',array("id" => $token));
            } else {
                $app->flash("error","La mise à jour de la liste a déjà été effectuée");
                //$v = new VueListe($errors);
                //$v->create();
            }
        } else {
            $app->flash("error", $errors);
        }
        $app->redirectTo('liste',array("id" => $token));
    }

    /**
     * Formulaire ajout item
     */
    public function formulaireAjoutItem($token,$error =null){
        $redirection = false ;
        $liste=Liste::where('token','=',$token)->first();
        if(isset($_SESSION['id'])) {
            if($_SESSION['id']!=$liste->idUser) {
                $redirection = true ;
                $_SESSION['messageErreur'] = "Cette liste ne vous appartient pas." ;
                $app = Slim::getInstance() ;
                $app->redirectTo('root') ;
            }
        }

        if(!$redirection)
            (new VueListe($liste,$error))->ajouterUnItem($token);
    }

    /**
     * Vérifie les champs du formulaire d'ajout d'item
     * @param string $token
     */
    public function VerifierChamps($token){
        $app=Slim::getInstance();

        $liste=(new ControleurCreateur())->vraiFonctionGetListe($token);

        $error=null;
        $idListe=null;
        foreach ($liste as $l){
            $idListe=$liste->id;
        }

        $nompost=$_POST['nomItem'];
        $descpost=$_POST['descriptionItem'] ;
        $urlpost=$_POST['url'];
        $tarifpost=$_POST['tarif'];
        $cagnottepost=$_POST['cagnotte'];

        // Vérification des POST
        if (isset($nompost)) {
            $nom = htmlspecialchars($nompost);
        }
        if (isset($descpost)) {

            $description = htmlspecialchars($descpost);
        }

        if (isset($urlpost)) {
            if(!empty($urlpost)) {
                if (filter_var($urlpost, FILTER_VALIDATE_URL)) {
                    $url = htmlspecialchars($urlpost);
                } else {
                    $app->flash("errors", "Le format de l'url n'est pas valide");
                    $app->redirectTo('ajoutItem', array("id" => $token));
                }
            } else {
                $url = null;
            }
        } else {
            $url = null;
        }

        if (isset($tarifpost)){
            if(filter_var($tarifpost, FILTER_VALIDATE_FLOAT) !== false){
                $tarif = htmlspecialchars($tarifpost);
            } else {
                $app->flash("errors","Le format du tarif n'est pas valide");
                $app->redirectTo('ajoutItem', array("id" => $token));
            }

        }

        if (isset($cagnottepost)) {
            $cagnotte = htmlspecialchars($cagnottepost);
        }

        $image = $this->verifierUpload() ;

        //var_dump($idListe);var_dump($nom);var_dump($description);var_dump($url);var_dump($tarif);var_dump($cagnotte);
        // Verification de la validité des informations
        if (!empty($nom) && !empty($description) && !empty($idListe) && !empty($tarif)) {
            $array=array('nom' => $nom, 'description' => $description, 'idListe' => $idListe, 'tarif' => $tarif, 'cagnotte' => $cagnotte, 'url' => $url, 'img'=>$image);

            return $array;
        } else {
            return null;
        }
    }

    /**
     * Pouvoir modifier les informations de la liste
     */
    public function ajouterItem($token){
        $app=Slim::getInstance();
        $array=$this->VerifierChamps($token);
        if($array != null){
            Item::creerItem($array['idListe'],$array['nom'],$array['description'],$array['url'],$array['tarif'],$array['cagnotte'], $array['img']);
            $app->redirectTo('liste',array("id" => $token));
        } else {
            $app->flash("error","Un champ a été mal renseigné");
            $app->redirectTo('ajoutItem', array("id" => $token));
        }
    }

    /**
     * Récupère les informations du formulaire de modification d'item
     * @param $tokenListe
     * @param $id
     */
    public function modifierItem($tokenListe,$id){
		$redirection = false ;
        $app = Slim::getInstance() ;
        $liste=Liste::where('token','=',$tokenListe)->first();
        if(isset($_SESSION['id'])) {
            if($_SESSION['id']!=$liste->idUser) {
                $redirection = true ;
                $_SESSION['messageErreur'] = "Cette liste ne vous appartient pas." ;
                $app->redirectTo('root');
            }
        }
        if(!$redirection) {
                $vue = new VueListe(null) ;
                $vue->modifierUnItem($tokenListe, $id) ;
        }
	}

    /**
     * Valide la modification des informations d'un item
     * @param $tokenListe
     * @param $idItem
     */
	public function validerItem($tokenListe,$idItem){
		 $array=$this->VerifierChamps($tokenListe);
        if($array != null){
            $item=Item::find($idItem);
			$item->idListe=$array['idListe'];
			$item->nom=$array['nom'];
			$item->descr=$array['description'];
			$item->url=$array['url'];
			$item->tarif=$array['tarif'];
			$item->cagnotte=$array['cagnotte'];
			$item->img=$array['img'] ;
			$item->save();
		}
		\Slim\Slim::getInstance()->redirectTo('liste', array("id" => $tokenListe));
	}

    /**
     * Gére la suppression d'un item d'une liste
     * @param $idListe
     * @param $idItem
     */
	public function supprimerItem($idListe,$idItem){
        $app=Slim::getInstance();
		$reservations=Participant::where('idListe','=',$idListe)->where('idItem','=',$idItem)->first();
		//messages d'erreurss
		if(isset($_SESSION['id'])){ // si l'utilisateur n'est pas connecté
			if($_SESSION['id']!=Liste::find($idListe)->idUser){ // si l'utilisateur ne possède pas la liste
				$_SESSION['messageErreur']="Permission refusée, cette liste ne vous appartient pas";
				$app->redirectTo("root");
			}
		}
		else{
				$_SESSION['messageErreur']="Vous n'êtes pas connecté, action impossible";
				$app->redirectTo("root");
			}
			
		
		if($reservations!=null){
			$_SESSION['messageErreur']="Permission refusée, cet item est reservé";
			$app->redirectTo("root");
		}
		if(Item::find($idItem)->idListe!=$idListe){
			$_SESSION['messageErreur']="Permission refusée, cet item n'appartient pas à cette liste";
			$app->redirectTo("root");
		}		
        $item=Item::where('id','=',$idItem)->where('idListe','=',$idListe)->first();
        unlink("img/".$item->img) ;
		$item->delete();
		$token=Liste::find($idListe)->token;
		
        $app->redirectTo('liste', array("id" => $token));

    }

    /**
     *Permet de vérifier la validité de l'image insérée
     * @return null|string nom de l'image enregistré dans la base
     */
    public function verifierUpload() {
	    $image = null ;
	    if(isset($_FILES['imgItem'])) {
            $file = $_FILES['imgItem'];

            //vérification erreur
            if ($file['error'] > 0)
                $image = null;

            //vérification format
            $extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
            $extension_upload = strtolower(substr(strrchr($file['name'], '.'), 1));
            if (!in_array($extension_upload, $extensions_valides))
                $image = null;

            //Déplacer fichier
            $nom = md5(uniqid(rand(), true));
            $image = "{$nom}.{$extension_upload}";
            $resultat = move_uploaded_file($file['tmp_name'], "img/".$image) ;
            if (!$resultat) {
                $image = null;
            }
        }

        return $image ;

    }
}