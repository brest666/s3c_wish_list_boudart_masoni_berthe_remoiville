<?php
namespace mywishlist\Controleurs;

use mywishlist\Modeles\Participant;
use mywishlist\Modeles\Utilisateur;
use mywishlist\Vues\VueCompte;
use mywishlist\Vues\VueListe;
use mywishlist\Modeles\Item ;
use mywishlist\Modeles\Liste ;
use mywishlist\Vues\VuePageAccueil;
use Slim\Slim;

/**
 * Class ControleurParticipant
 * Gére les actions d'un participant (un non créateur de la liste manipulée)
 * @package mywishlist\Controleurs
 */
class ControleurParticipant {

    /**
     * Redirige vers la page d'un item, si celui-ci appartient à une liste valide alors il s'affiche
     * sinon redirection vers la page d'accueil avec un message d'erreur
     * @param $tokenL
     * @param $idI
     */
	public function getItem($tokenL,$idI){
	    $idListe = Liste::where('token', '=', $tokenL)->first() ;
        $dateCourante = new \DateTime();//éventuellement ajouter 1 semaine de rab
        $dateListe = new \DateTime($idListe->expiration) ;
        if($dateCourante>$dateListe) {
                $app = Slim::getInstance() ;
                $_SESSION['messageErreur'] = "Cet item appartient à une liste expirée" ;
                $app->redirectTo('root') ;
        }
        else {
            $item = Item::where('id', '=', $idI)->where('idListe', '=', $idListe->id)->first();
            $item->nomListe = $idListe->titre;
            $vue = new VueListe ($item);
            $vue->etreExpiree($idListe->expiration);
            $vue->render(VueListe::AFF_ONE_ITEM);
        }
	}

    /**
     * Gère la réservation d'items dans une liste
     * @param $tokenL
     * @param $idI
     */
	public function reserver($tokenL,$idI){
	    $redirection = false ;
	    $liste = Liste::where('token', '=', $tokenL)->first();
	    if(isset($_SESSION['id'])) {
	        if($_SESSION['id']==$liste->idUser) {
	            $redirection = true ;
	            $_SESSION['messageErreur'] = "Vous ne pouvez pas réserver un item appartenant à votre liste" ;
	            $app = Slim::getInstance() ;
	            $app->redirectTo('root') ;
            }
        }

        if(!$redirection) {
            $vue = new VueListe (null);
			$_SESSION['ResListe']=$tokenL;
			$_SESSION['ResItem']=$idI;
            $vue->render(VueListe::AFF_RESERVATION);
        }
	}

	
	public function validerReservation(){
		var_dump($_POST);
		
		$identifiant=$_POST['identifiant'];
		$message=$_POST['message'];
		if(isset($_POST['tarif'])){
			$tarif=$_POST['tarif'];	
		}
		else{
			$tarif=NULL;
		}
		
		$p = new Participant();
        $p->participant = $identifiant;
        if(isset($_SESSION['id']))
            $p->idUser = $_SESSION['id'];
        else
            $p->idUser = 0 ;
        $p->idItem = $_SESSION['ResItem'];
        $p->idListe = Liste::where('token','=',$_SESSION['ResListe'])->first()->id;
        $p->texte = $message;
        $p->tarif = $tarif;
        $p->date = date("d-m-Y");
        $p->save();
		
		$app = Slim::getInstance();
	    $app->redirectTo('liste',array('id'=>$_SESSION['ResListe'])) ;
		
	}
	
	
	//on passe le TOCKEN en parametre 


    /**
     * Redirige vers l'affichage d'une liste avec ses items
     * Ou redirige vers l'accueil avec un message d'erreur si la liste a expirée
     * @param $id
     */

	public function getListe($id){
		$liste=Liste::where('token','=',$id)->first();
        $dateCourante = new \DateTime();//éventuellement ajouter 1 semaine de rab
        $dateListe = new \DateTime($liste->expiration) ;
		if($dateCourante>$dateListe) {
            $app = Slim::getInstance() ;
            $_SESSION['messageErreur'] = "La liste que vous souhaitez visualiser est expirée" ;
            $app->redirectTo('root') ;
        }
        else {
            $vue = new VueListe ($liste);
            $vue->etreExpiree($liste->expiration) ;
            $vue->render(VueListe::AFF_ITEMS_OF_LIST);
        }
	}

	/**
	* Permet de n'aficher que les listes publiques 
	* et qui n'ont pas encore été expirées
	*/
	public function getListes(){
		$listes=Liste::listesPubliques() ;
		$vue = new VueListe ( $listes );
		$vue->render(VueListe::AFF_ALL_LISTS);
	}

    /**
     * Ajouter un message sur le chat d'une liste par un participant
     * @param $id
     */
	public function ajoutMessage($id) {
        $_SESSION['participant'] = $_POST['participant'];
        $name = $_POST['participant'];
        $texte = htmlspecialchars($_POST['texte']) ;
        $idU = 0;
        if (isset($_SESSION['id'])) {
            $idU = $_SESSION['id'];
        }
        $date = date('Y-m-d');

        $p = new Participant();
        $p->participant = $name;
        $p->idUser = $idU;
        $p->idItem = 0;
        $p->idListe = $id;
        $p->texte = $texte;
        $p->tarif = 0;
        $p->date = $date;
        $p->save();

        $app = Slim::getInstance();
        $token = Liste::where('id', '=', $id)->first()->token;
        $app->redirectTo('liste', array('id' => $token));
    }

    /**
     * Vue d'un compte par un utilisateur extérieur
     * @param $id
     */
    public function voirCompte($id) {
        $vue = new VueCompte(Utilisateur::getCompte($id)) ;
        $vue->render(VueCompte::AFF_COMPTE_VUE_EXTERIEURE) ;
    }
}
