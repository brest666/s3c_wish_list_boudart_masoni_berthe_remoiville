<?php
/**
 * Le créateur peut :
 *  Doit se connecter
 *  Créer une liste
 *  Gérer une liste
 *      Pouvoir ajouter des items
 *      Pouvoir supprimer des items
 *      Pouvoir consulter sa liste
 *  Partager la liste
 */
namespace mywishlist\Controleurs;

use mywishlist\Modeles\Participant;
use mywishlist\Modeles\Utilisateur;
use mywishlist\Vues\VueCompte;
use mywishlist\Vues\VueErreur;
use mywishlist\Vues\VueListe;
use mywishlist\Modeles\Liste ;
use mywishlist\Modeles\Item ;
use mywishlist\Vues\VuePageAccueil;
use Slim\Slim;

/**
 * Class ControleurCreateur
 * Un créateur est celui qui a créé une liste et qui ne peut pas participer à sa propre liste mais peut la gérer
 * @package mywishlist\Controleurs
 */
class ControleurCreateur {

    /**
     * Execute le render de l'ensemble des listes du créateur
     * Sinno redirige l'information sur le controleur de Participant
     * @param $id
     */
    public function getListe($id)
    {
        $liste=Liste::where('token','=',$id)->first();
        if($liste->idUser!=$_SESSION['id']) {
            $c = new ControleurParticipant() ;
            $c->getListe($id) ;
        }
        else {
            $vue = new VueListe($liste);
            $vue->etreExpiree($liste->expiration) ;
            $vue->render(VueListe::AFF_LIST_CREATEUR);
        }
    }

    /**
     * Execute le render de l'ensemble des items selon s'il s'agit du créateur ou du participant
     * @param $tokenL
     * @param $idI
     */
    public function getItem($tokenL, $idI) {
        $liste=Liste::where('token','=',$tokenL)->first();
        if($liste->idUser!=$_SESSION['id']) {
            $c = new ControleurParticipant() ;
            $c->getItem($tokenL, $idI) ;
        }
        else {
            $idListe = Liste::where('token', '=', $tokenL)->first() ;
            $item=Item::where('id','=',$idI)->where('idListe', '=', $idListe->id)->first();
            $item->nomListe = $idListe->titre ;
            $vue = new VueListe ( $item );
            $vue->etreExpiree($idListe->expiration) ;
            $vue->render(VueListe::AFF_ITEM_CREATEUR);
        }
    }

    /**
     * Affiche le compte selon l'utilisateur (s'il est propiétaire ou visiteur)
     * @param $id
     */
    public function voirCompte($id) {
        $vue = new VueCompte(Utilisateur::getCompte($id)) ;
        if($_SESSION['id']==$id) {
            $vue->render(VueCompte::AFF_COMPTE_VUE_PROPRIETAIRE) ;
        }
        else {
            $vue->render(VueCompte::AFF_COMPTE_VUE_EXTERIEURE) ;
        }
    }

    /**
     * Gère l'affichage du formulaire de modification des informations de la liste
     * @param $id
     */
    public function modifierInformationsListe($id){
        $redirection = false ;
        $liste=$this->vraiFonctionGetListe($id);
        if(isset($liste)) {
            if(isset($_SESSION['id'])) {
                if ($_SESSION['id'] != $liste->idUser) {
                    $redirection = true;
                    $app = Slim::getInstance();
                    $_SESSION['messsageErreur'] = "Cette liste ne vous appartient pas.";
                    $app->redirectTo('root');
                }
            }

            if(!$redirection) {
                $v = new VueListe($liste);
                $v->modification($liste);
            }
        } else {
            $v = new VueErreur();
            $v->erreur404();
        }
    }

    /**
     * Retourne la liste correspondant à l'id
     * @param $id
     * @return mixed
     */
    public function vraiFonctionGetListe($id){
        return $liste=Liste::where('token','=',$id)->first();
    }

   
    public function supprimerCompte(){
        $app = Slim::getInstance() ;
        if(isset($_SESSION['id'])) {
            
            $compte = Utilisateur::find($_SESSION['id']) ;

            $participations = Participant::where('idUser','=',$compte->id)->get() ;
            foreach ($participations as $p) {
                $p->delete() ;
            }

            $listes = Liste::where('idUser','=',$compte->id)->get() ;
            foreach ($listes as $l) {
                $items = Item::where('idListe','=',$l->id)->get() ;
                foreach ($items as $i) {
                    $i->delete() ;
                   
                }
               
                $l->delete() ;
            }

            $compte->delete() ;
			unset($_SESSION['id']);
            $app->redirectTo('root');
        }
        else {
            $_SESSION['messageErreur'] = 'Cette fonctionnalité n\'est utilisable que par les utilisateurs enregistrés';
            $app->redirectTo('root') ;
        }
    }
}
















