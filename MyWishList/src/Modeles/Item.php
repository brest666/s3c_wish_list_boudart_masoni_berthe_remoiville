<?php
/**
 * Un Item a :
 *  Un nom ....
 */

namespace mywishlist\Modeles;

use Slim\Slim;

/**
 * Class Item
 * Modèle de la table item
 * @package mywishlist\Modeles
 */
class Item extends \Illuminate\Database\Eloquent\Model{

    /**
     * ATTRIBUTS
     * @var string
     */
	protected $primaryKey = 'id';
	protected $table = 'item';
	public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function liste(){
		  return $this->belongsTo('mywishlist\Modeles\Liste','idListe');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participant() {
        return $this->hasMany('mywishlist\Modeles\Participant', 'idItem') ;
    }

    /**
     * Liste l'ensemble des participants de l'item
     * @return array
     */
    public function participants() {
        $p = Participant::where('idItem', '=', $this->id)->get() ;
        $reserve = false ;

        if($this->cagnotte==true) {
            $prix = $this->tarif ;
            $somme = 0 ;
            foreach ($p as $t) {
                $somme += $t->tarif;
            }
            if ($somme == $prix)
                $reserve = true;
        }
        else {
            if(count($p)==1)
                $reserve = true ;
        }

        $part = array('reserve'=>$reserve, 'participants'=> $p) ;

        return $part ;
    }

    /**
     * Retourne le nom d'un item
     * @param $idI
     * @return mixed
     */
    public static function getNom($idI) {
	    return Item::select('nom')->where('id', '=', $idI)->first() ;
    }

    /**
     * Créer un item
     * @param $idListe
     * @param $nom
     * @param $description
     * @param null $url
     * @param $tarif
     * @param $cagnotte
     */
    public static function creerItem($idListe,$nom,$description,$url=null,$tarif,$cagnotte,$image){
        $i=new Item();
        $i->id=Item::idMax();
        $i->idListe=$idListe;
        $i->nom=$nom;
        $i->descr=$description;
        $i->url=$url;
        $i->tarif=$tarif;
        $i->cagnotte=$cagnotte;
        $i->img=$image;
        if(!Item::existeDejaDansListe($nom,$idListe)) {
            $i->save();
        } else {
            $token=Liste::select('token')->where('id','=',$idListe)->first();
            if(!empty($token)){
                $app=Slim::getInstance();
                $app->flash("errors","L'item existe déjà dans la liste");
                $app->redirectTo('ajoutItem', array("id" => $token));
            }
        }
    }

    /**
     * Vérifie qu'un item n'est pas déjà présent dans la liste avec le même nom
     * @param $nomItem
     * @param $idListe
     * @return bool
     */
    public static function existeDejaDansListe($nomItem,$idListe){
        $existe=Item::select('*')->where('idListe','=',$idListe)
            ->where('nom','=',$nomItem)->first();
        if(!($existe == null)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retourne l'id maximal de la table item
     * @return mixed
     */
    public static function idMax(){
        $idmax=Item::select('id')->whereRaw('id= (select max(id) from item)')->first()->id;
        return $idmax+1;
    }
}
