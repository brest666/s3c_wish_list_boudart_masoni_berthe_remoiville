<?php

namespace mywishlist\Modeles;


use mywishlist\Vues\VueConnexion;
use Slim\Slim;

/**
 * Class Authentification
 * @package mywishlist\Modeles
 */
class Authentification
{
    /**
     * @var
     */
    protected $authSessinVar;

    /**
     * Créer un utilisateur
     * @param $userName
     * @param $password
     */
    public static function createUser($userName,$password,$email){
        // vérifier la conformité de $password avec la police
        // si ok : hacher $password
        // créer et enregistrer l'utilisateur

		//POUR LE MOMENT J'ENREGISTRE JUSTE DANS LA BASE ;)

        $u = new Utilisateur();
		$id=Utilisateur::select('id')->whereRaw('id= (select max(id) from compte)')->first()->id;
		$id++;
		$u->id=$id;
		$u->nom=$userName;
		$u->mail=$email;
		$u->mdp=password_hash($password,PASSWORD_DEFAULT,['cost'=>12]);
		$u->save();
    }

    /**
     * Réaliser l'authentification
     * Contrôler l'identifiant et le mot de passe
     */
    public static function authenticate($userName,$password){
        $valid_name=false;
        $valid_pass = false;
        $valide = false;
        $errors=null;
        // initialisation
        $req=null;

        // Recherche du userName dans la BDD
        if(isset($userName)) {
            $req = Utilisateur::select('id')
                ->where('nom', '=', $userName);
            if($req->first() == null){
                $email = $userName;
                $req = Utilisateur::select('id')
                    ->where('mail', '=', $email);
            }
        }

        if($req->first() == null){
            $errors.="<p>L'identifiant est invalide</p>";
        } else {
            $valid_name = true;
            $id = $req->first()->id;
        }


        // Vérifie que le password a été saisi
        if(isset($password) && $valid_name) {

            //================
            // password dans BDD
            $req2 = Utilisateur::select('mdp')
                ->where('id', '=' , $id);
            $BDD_pass = $req2->first()->mdp;
            // ===============

            // verify password
            if($BDD_pass!=null) {

                if (password_verify($password, $BDD_pass)) {
                    $valid_pass = true;
                } else {
                    $errors.="<p>Mot de passe incorrect</p>";
                }
            }
			
            if ($valid_pass && $valid_name) {
				$session = self::loadProfile($id);
                $valide = true;
            }
        } 
        $app=Slim::getInstance();
        if($valide){
            $app->redirectTo('root');
        } else {
            $app=Slim::getInstance();
            $app->flash('errors', $errors);
            $app->redirectTo('connexion');
        }
    }

    /**
     * @param $uidcharger le profil d'un utilisateur en session
     */
    public static function loadProfile($uid){
        if(isset($_SESSION['id']))
            unset($_SESSION['id']) ;
        $_SESSION['id'] = $uid;
        $_SESSION['level'] = 0;
        echo "<br/>" . ($_SESSION['id']);

    }
}