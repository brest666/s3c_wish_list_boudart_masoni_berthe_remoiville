<?php

namespace mywishlist\Modeles;

/**
 * Class Utilisateur
 * Modélise la table compte
 * @package mywishlist\Modeles
 */
class Utilisateur extends \Illuminate\Database\Eloquent\Model{

    /**
     * ATTRIBUTS
     */
    protected $primaryKey = 'id';
    protected $table = 'compte';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listes(){
        return $this->belongsTo('mywishlist\Modeles\Liste','idUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function participations() {
        return $this->belongsTo('mywishlist\Modeles\Participant', 'idUser') ;
    }

    /**
     * Retourne le nom de la session de l'utilisateur
     * @return null
     */
    public static function nomSession() {
		$nom=null;
		if (isset($_SESSION['id'])) {
                $nom = Utilisateur::where('id', '=', $_SESSION['id'])->first()->nom;
            }
            if (isset($_SESSION['participant']))
                $nom = $_SESSION['participant'];
        return $nom;
    }

    /**
     * Retourne l'ensemble des créateurs connectés
     * @return array
     */
    public static function createurActif() {
        $liste = Liste::listesPubliques() ;
        $createurs = array() ;
        $i = 0 ;
        foreach ($liste as $l) {
            $createurs[$i] = Utilisateur::where('id', '=', $l->idUser)->first() ;
            if(isset($_SESSION['id'])) {
                if ($createurs[$i]->id == $_SESSION['id']) {
                    unset($createurs[$i]) ;
                }
                else {
                    $i++ ;
                }
            }
            else {
                $i++ ;
            }
        }
        return $createurs ;
    }

    /**
     * Retourne le compte correspondant à l'id en paramètre
     * @param $id
     * @return mixed
     */
    public static function getCompte($id) {
        return Utilisateur::where('id', '=', $id)->first() ;
    }
}