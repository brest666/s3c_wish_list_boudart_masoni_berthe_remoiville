<?php
/**
 * Created by PhpStorm.
 * User: Océane
 * Date: 14/12/2017
 * Time: 19:31
 */

namespace mywishlist\Modeles;

/**
 * Class Participant
 * Modélise la table participant
 * @package mywishlist\Modeles
 */
class Participant extends \Illuminate\Database\Eloquent\Model
{

    /**
     * ATTRIBUTS
     */
    protected $primaryKey = 'id';
    protected $table = 'participant';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function utilisateur(){
        return $this->hasMany('mywishlist\Modeles\Utilisateur','idUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function liste(){
        return $this->hasMany('mywishlist\Modeles\Liste','idListe');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function item(){
        return $this->hasMany('mywishlist\Modeles\Item','idItem');
    }

    /**
     * Retourne l'ensemble des messages d'une liste
     * @param $id
     * @return mixed
     */
    public function messagesListe($id) {
        return Participant::where('idListe', '=', $id)->where('idItem', '=', 0)->orderBy('date')->get() ;
    }

    /**
     * Retourne l'ensemble des participants d'une liste
     * @param $id
     * @return mixed
     */
    public static function participations($id) {
        return Participant::where('idUser', '=', $id)->where('idItem', '!=', 0)->get() ;
    }
}