<?php

namespace mywishlist\Modeles;

use Slim\Slim;

/**
 * Class Liste
 * @package mywishlist\Modeles
 */
class Liste extends \Illuminate\Database\Eloquent\Model{

    /**
     * ATTRIBUTS
     */
	protected $primaryKey = 'id';
	protected $table = 'liste';
	public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function items(){
		  return $this->hasMany('mywishlist\Modeles\Item','idListe');
	}

    /**
     * Retourne le propriétaire de la liste
     * @return mixed
     */
    public function proprietaire(){
        return Utilisateur::where('id', '=', $this->idUser)->first() ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function messages(){
        return $this->belongsTo('mywishlist\Modeles\Participant','idListe');
    }

    /**
     * Enregistre une liste dans la base de données
     * @param $nom
     * @param $description
     * @param $expiration
     */
    public static function createListe($nom, $description, $expiration,$public){
        $liste=new Liste();

        if(isset($_SESSION['id'])) {
            $liste->idUser=$_SESSION['id'];
        }
        else {
            return false;
        }
        $liste->id=Liste::idMax();
        $liste->titre=$nom;
        $liste->description=$description;
        $liste->expiration=$expiration;
		$liste->public=$public;
		$liste->token=uniqid();
        if(!$liste->exist()) {
            $liste->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que la liste en paramètre n'ait pas déjà été créée par le même utilisateur et ne porte pas le même titre
     * @param $liste_a_tester
     * @return boolean
     */
    public function exist(){
        $requete=Liste::all();
        $exist=false;
        foreach($requete as $r){
            if(($r['idUser'] == $this->idUser) && ($r['titre'] == $this->titre)){
                $exist=true;
            }
        }
        return $exist;
    }

    /**
     * Permet de sélectionner les listes publiques non expirées
     * @return mixed
     */
    public static function listesPubliques() {
        $listes =  Liste::where('public', '=', true)->orderBy('expiration')->get() ;

        //Ne pas afficher les listes dont la date d'expiration est dépassée
        $dateCourante = new \DateTime() ;
        foreach($listes as $i=>$liste) {
            $exp = new \DateTime($liste->expiration) ;
            if($dateCourante>$exp)
                unset($listes[$i]) ;
        }

        return $listes ;
    }

    /**
     * Retourne l'ensemble des listes d'un utilisateur
     * @param $id
     * @return mixed
     */
    public static function getUserLists($id) {
        return Liste::where('idUser', '=', $id)->orderBy('expiration')->get() ;
    }

    /**
     * Retourne la liste dont l'id est celui renseigné en paramètre
     * @param $idL
     * @return mixed
     */
    public static function getList($idL) {
        return Liste::where('id', '=', $idL)->first() ;
    }

    /**
     * Retourne l'id+1 le plus grand de la table
     * @return mixed
     */
    public static function idMax(){
        $idmax=Item::select('id')->whereRaw('id= (select max(id) from liste)')->first()->id;
        return $idmax+1;
    }
}
